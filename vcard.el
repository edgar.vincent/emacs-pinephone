;;; An emacs file to read and write from a vcard file since that is apparently
;;; a good way of storing contact info. I will need to look more into this because
;;; it can also store photos and whatnot but I don't know how to deal with that.

;; Faces
(defface contact-info
  '((((class color) (background light))
     :foreground "DarkGrey" :weight bold :height 2.0 :inherit variable-pitch)
    (((color class) (background dark))
     :foreground :darkgoldenrod3 :weight bold :height 2.0 :inherit variable-pitch)
    (t :height 1.5 :weight bold :inherit variable-pitch))
  "Contact Face"
  :group 'contacts)

;;;The above face is basically just straight from Calfw because that is how I found out
;;;about it but also the link below has a nifty breakdown of it.
;;; https://protesilaos.com/codelog/2020-08-28-notes-emacs-theme-devs/

;;; Here is an example of what a contact looks like in vcf format


;; BEGIN:VCARD
;; VERSION:3.0
;; TEL;TYPE=CELL:XXX
;; CATEGORIES:myContacts
;; END:VCARD
;; BEGIN:VCARD
;; VERSION:3.0
;; FN:👻
;; N:;👻;;;
;; TEL;TYPE=CELL:XXX
;; TEL;TYPE=CELL:XXX
;; CATEGORIES:myContacts
;; END:VCARD

;; BEGIN:VCARD                       Always the same
;; VERSION:3.0                       List the version number 
;; FN:Alec (El Carpoolo)             Formatted name
;; N:;Alec (El Carpoolo);;;          Structured representation of the name? Optional in v4
;; TEL;TYPE=CELL:XXX                 Canonical number string for phone number
;; CATEGORIES:myContacts             Basic descriptive tages
;; END:VCARD                         Always the same

;;;;;;;;;;; Should probably store stuff as vcard version 4 rather than version 3? ;;;;;;;;

;https://en.wikipedia.org/wiki/VCard

;;; What I can also try to do is load in the file to a list then use add-to-list so that
;;; it will automatically avoid duplicates?

(defun add-contact (FN N TEL)
  "The only things the user needs to input is the formated name
  the structured name and the number. I will just default the rest"
  (setq begin "BEGIN:VCARD")
  (setq version "VERSION:4.0")
  (setq categories "CAREGORIES:contacts")
  (setq end "END:VCARD")
  (setq contact_info (concat "\n" begin "\n" version "\n" FN "\n" N "\n" TEL "\n" categories "\n" end "\n"))
  (condition-case nil
      (progn
	(append-to-file contact_info nil "test.vcf")
	(message "Contact added, %s" N)
	)
    (error
     (message "Ruh roh, that didn't work!")     
     )
    ;;;AT THE END HERE I WOULD CALL THE CHECK-DUPLICATES FUNCTION TO CLEAR OUT ANY DUPES
  )
)


(defun read-contacts ()
  (switch-to-buffer-other-window "Contacts")
  (insert-file-contents "test.vcf" nil))
(read-contacts)
;;;I need to filter this so it'll just show the name, and number
;;;might also need to add an email slot
;;;could just do a for loop and say insert what comes after N and TEL?
;;;and have it be like number:TEL, Name:N
;;;I would also like to be able to add contrasting lines to make it more clear

(defun check-duplicate (FN1 FN2)
  "make sure there aren't duplicates"
  (setq output (string= FN1 FN2))
  (if (string= output nil)
      (message "These are different")
    )
  )

(with-temp-buffer
  (insert-file-contents "test.vcf")
  (let ((list '()))
    (while (not (eobp))
      (let ((beg (point)))
	(move-end-of-line nil)
	(push (split-string (buffer-substring beg (point)) " ") list)
	(forward-char)))
    (nreverse list))
  )

(with-current-buffer (find-file-noselect "test.vcf")
  (mapcar (lambda (x) (split-string x " " t))
	  (split-string
	   (buffer-substring-no-properties (point-min) (point-max))
	   "\n")))

(defun file-to-string (file)
  "Converts files to strings"
  (setq contact_list (list ))
  (with-temp-buffer
    (insert-file-contents file)
    (contact_list (append '(insert-file-contents file))))
  (message "%S" contact_list))


;;This is how to deal with the list. Thanks to xahlee.info/emacs/emacs/elisp_list.html
(setq contact-list (list ))
(message "%S" contact-list)

(defun test-list ()
  (setq my-list '())
  (add-to-list 'my-list '1)
  (message "%S" my-list))
(setq list1 '(1 2 3))
(setq list1 (append 5))

(defun fuck ()
  (setq list1 '(1))
  (setq list2 (append list1 4))
  (setq list3 (append list1 list2))
  (message "%S" list3))
(fuck)

;; ------------------------ TESTS --------------------------- ;;
(check-duplicate "Value 1" "Value 2")


(add-contact "Hank" "Hank" "+14802522653")


(file-to-string "test.vcf")

(test-list)
