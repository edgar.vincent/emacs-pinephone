;;; In Progress: Need to add the exception handling so this would work for both PlaMo and Phosh.
;;; TODO: Add more specific errors.
;;; I need the name of the PlaMo counterparts so I can also try and launch them as well.


(defun mmcli-send-sms (sms number)
  "Sends a text message via mmcli"

  (setq file-name "%s-sms.txt" (current-time-string))
  (shell-command-to-string "mmcli -m any messaging-create-sms='text='%s'=,number='%d''>>%s" sms number file-name)
  ;;(find-file "/home/hank/Development/elisp/info.txt")
  (setq val (insert-file-contents "%s" file-name nil 0 100))
  (shell-command-to-string "mmcli -m any -s %d --send" val)
  ;;; Save this to file then open the file and read back the output and take the number at the end and send the message
  ;;; the message is sent with mmcli -m any -s NUMBER --send
  ;;; use write-file and find-file to get the info
  (switch-to-buffer-other-window "Message Sent")
  (insert "%s sent to %d" sms number)
)

;;(mmcli-send-sms "Blahblah blah" 543)


(defun make-call (number)
  "Make a phone call via gnome-calls"
  (shell-command-to-string "gnome-calls --dial=%d" number)
)

(defun chatty ()
  "Launches chatty"
  (shell-command-to-string "chatty")
)

(defun calls ()
  "Launch calls"
  (shell-command-to-string "gnome-calls")
)

(defun firefox()
  "launch firefox"
  (shell-command-to-string "firefox")
)

(defun megapixels ()
  "launch megapixels"
  (shell-command-to-string "megapixels")
)

(defun contacts ()
  "launch gnome-contacts"
  (shell-command-to-string "gnome-contacts")
)

(defun settings ()
  "Launch gnome settings"
  (shell-command-to-string "gnome-control-center")
)

;;; need to add sudo in here, stackoverflow has an answer
;;; https://stackoverflow.com/questions/11955298/use-sudo-with-password-as-parameter

(defun install-packages (password package-name)
  "wrapper for install pacman packages"
  (shell-command-to-string "echo %s | sudo -S pacman -S %s" password package-name)
)

(defun uninstall-packages (password package-name)
  "wrapper for install pacman packages"
  (shell-command-to-string "echo %s | sudo -S pacman -R %s" password package-name)
)


;;; Below here are basically the above function but with exception handling. Can't really test
;;; these much without the keyboard attachment for the PP so they won't replace the above functions
;;; until then. By convention they will all start with test int he name

(defun test-install-packages (password package-name)
  "Wrapper for installing pacman packages with exception handling?"
  (condition-case nil
      (progn
	(shell-command-to-string "echo %s | sudo -S pacman -S %s" password package-name))
    (error
     (message "ruh roh, doesn't work.")
    )
  )
)

(defun test-uninstall-packages (password package-name)
  "Wrapper for installing pacman packages with exception handling?"
  (condition-case nil
      (progn
	(shell-command-to-string "echo %s | sudo -S pacman -R %s" password package-name))
    (error
     (message "ruh roh, doesn't work.")
    )
  )
)


(defun test-make-call (number)
  "Make a phone call via gnome-calls"
  (condition-case nil
      (progn
	(shell-command-to-string "gnome-call --dial=%d" number))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)


;;; This is say if chats fails then try and launch spacebar,
;;; if that fails then spit out an error message
(defun test-chatty ()
  "Launches chatty"
  (condition-case nil
      (progn
	(setq chats (shell-command "chatty"))
	(if (= chats 127)
	    (shell-command "spacebar")))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)

(defun test-calls ()
  "Launches gnome-calls"
  (condition-case nil
      (progn
	(shell-command-to-string "gnome-calls"))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)

(defun test-firefox ()
  "Launches firefox"
  (condition-case nil
      (progn
	(shell-command-to-string "firefox"))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)

(defun test-firefox-search (query)
  "Launches firefox with search"
  (condition-case nil
      (progn
	(shell-command-to-string "firefox --search %s" query))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)

(defun test-megapixels ()
  "Launches megapixels"
  (condition-case nil
      (progn
	(shell-command-to-string "megapixels"))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)

(defun test-contacts ()
  "Launches gnome-contacts"
  (condition-case nil
      (progn
	(setq contacts (shell-command "gnome-contacts"))
	(if (= contacts 127)
	    (shell-command "plasma-phonebook")))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)

(defun test-settings ()
  "Launches gnome-settings"
  (condition-case nil
      (progn
	(setq settings (shell-command "gnome-control-center"))
	(if (= settings 127)
	    (shell-command "plasma-settings")))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)

;;;;;;;;;;;;;;;;; Config stuff ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(set-default-font "Monaco 14")

(require 'bbdb)
(bbdb-initialize 'message)
(bbdb-insinuate-message)
(add-hook 'message-setup-hook 'bbdb-insinuate-mail)
(require 'calfw)
(require 'calfw-ical)
(cfw:open-ical-calendar "path/to/calendar.ics")
(require 'mu4e)
(require 'smex) ; Not needed if you use package.el
(smex-initialize) ; Can be omitted. This might cause a (minimal) delay
                    ; when Smex is auto-initialized on its first run.

(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)



(use-package dtache
  :hook (after-init . dtache-initialize)
  :config
  (setq dtache-dr-directory user-emacs-directory)
  (setq dtache- session-directory (expand-file-name "dtache" (temporary-file-directory))))


;;; to use dtache in a function would be something like below

(defun dtache-test-chatty ()
  "Launches chatty"
  (condition-case nil
      (progn
	(setq chats (dtache-start-session shell-command "chatty"))
	(if (= chats 127)
	    (dtache-start-session shell-command "spacebar")))
    (error
     (message "Ruh roh, that didn't work!")
    )
  )
)


;;; The above config will take care of emails, detaching shell scripts (very important)
;;; contact list and calendar.

;;; Need to double check that none of these are preset ones.
(global-set-key (kbd "C-c C-h") 'chatty)
(global-set-key (kbd "C-c C-p") 'calls)
(global-set-key (kbd "C-c C-f") 'firefox)
(global-set-key (kbd "C-c C-m") 'megapixels)
